<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;

class CommentController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
        'body'         => 'required',
        'article_id'   => 'required' 
        ]);

        Comment::create($request->all());
        
        return back()->withMessage('Comment Berhasil Di Simpan');
    }
}
