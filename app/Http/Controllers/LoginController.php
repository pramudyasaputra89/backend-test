<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;

class LoginController extends Controller
{
    public function view()
    {
        return view('login');
    }

    public function login(Request $request)
    {
        $request->validate([
        'email'   => 'required|email',
        'password'=> 'required'
        ]);

        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            return redirect()->route('article.index');
        } else {
            // return 'salah';
            return back()->withMessage('Email/Password Salah');
        }
    }
}
