<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use App\Comment;
use Auth;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $number_of_pages = round(Article::count() / 10);
        return view('article-cms',['number_of_pages'=>$number_of_pages]);
    }

    /**
     * Return list of articles for ajax.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request)
    {
        $articles = Article::orderBy('created_at','desc')
        ->limit(10)
        ->offset(($request->skip - 1) * 10)
        ->get();
        
        return $articles->toArray();
    }

    /**
     * Return detail of article for ajax.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function detail(Request $request)
    {
        $article = Article::with('creator')
        ->where('id',$request->id)
        ->get();

        return response()->json(['code'=>'200','data'=>$article]);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('article-add');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
        'name' => 'required',
        'body' => 'required',
        ]);

        Article::create([
        'name'      => $request->name,
        'body'      => $request->body,
        'slug'      => str_slug($request->name),
        'creator'   => Auth::user()->id,
        ]);

        return redirect()->route('article.index')->withMessage('Data Berhasil Di Simpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {    
        $article  = Article::findOrFail($id);
        $comments = Comment::where('article_id',$article->id)
        ->get();

        return view('article', compact('article', 'comments'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $do = Article::find($id);
        $comment = Comment::where('article_id',$do->id);

        $do->delete();
        $comment->delete();

        return json_encode(true);
    }
}
