<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
	protected $guarded = ['id'];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function creator()
    {
    	return $this->hasOne('App\User','id','creator');
    }
}
