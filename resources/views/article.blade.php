<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Article CMS</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
    <div class="container">
        {{-- @if ($errors->any())
            @foreach($errors->all() as $error)
            <div class="col-md-4">
                <div class="alert alert-danger">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>{{ $error }}</strong>
                </div> 
            </div>
            @endforeach
        @endif --}}

        <div class="breadcrumb" style="margin-top:30px">
            <div class="col-md-12">
            <h3>{{ $article->name }}</h3>
            <hr>
            <p>{{ $article->body }}</p>
            <p class="text-right">{{ date('d F Y',strtotime($article->created_at)) }}</p>
        </div>
        </div>
        

        <div class="col-md-8 offset-md-2">
             <h4 class=""> Add Comment</h4>
            <form action="{{ route('comment.store') }}" method="POST">
                @csrf
                <input type="hidden" name="article_id" value="{{ $article->id }}">
                @error('body')
                <small>{{ $message }}</small>
                @enderror
                <textarea class="form-control" name="body"></textarea>
                <br>
                <input type="submit" class="btn btn-primary" value="Submit" />
            </form>
        </div>

        <hr>
        <h2>Comments</h2>
        
        @foreach($comments as $comment)
            <div class="card">
                <div class="card-body">{{ $comment->body }}</div>
            </div>
            <br><br>
        @endforeach
    </div>
</body>
</html>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>