<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Article CMS</title>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}" crossorigin="anonymous">
</head>
<body>
    <div class="container">

        @if ($errors->any())
            @foreach($errors->all() as $error)
            <div class="col-md-4">
                <div class="alert alert-danger">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>{{ $error }}</strong>
                </div> 
            </div>
            @endforeach
        @endif
        
        <form action="{{ route('login') }}" method="POST">
            @csrf
            <div class="col-md-6 offset-md-3" style="margin-top:120px">
                <div class="card">
                    <div class="card-header">
                        <h3>Login Form</h3>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label for="">Email</label>
                            <input type="text" name="email" class="form-control" placeholder="email" required="" />
                        </div>
                        <div class="form-group">
                            <label for="">Password</label>
                            <input type="password" name="password" class="form-control" placeholder="password" required="" />
                        </div>
                        <input type="submit" class="btn btn-primary btn-block" value="Submit"/>
                    </div>
                 </div>   
            </div>
        </form>
    </div>
</body>
</html>

<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="{{ asset('js/app.js') }}"></script>