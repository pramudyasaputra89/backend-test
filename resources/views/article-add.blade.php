<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Article CMS</title>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}" crossorigin="anonymous">
</head>
<body>
    <div class="container">
         @if ($errors->any())
            @foreach($errors->all() as $error)
            <div class="col-md-4">
                <div class="alert alert-danger">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>{{ $error }}</strong>
                </div> 
            </div>
            @endforeach
        @endif
        <div class="col-md-12">
            <h1>Add Article</h1>
            
            <form action="{{ route('article.store') }}" method="POST">
                {{ csrf_field() }}   
                <div class="card">
                    <div class="card-header">
                        <h4>Form Article</h4>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label>Article Name</label>
                            <input type="text" name="name" class="form-control" />
                        </div>
                        <div class="form-group">
                            <label>Article Body</label>
                            <textarea type="text" name="body" class="form-control"></textarea>
                        </div>
                         <input type="submit" class="btn btn-primary btn-block" value="Submit" />
                    </div>
                </div>
            </form>
        </div>
    </div>
</body>
</html>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>