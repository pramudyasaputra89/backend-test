<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Article CMS</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>

<body>
    <div class="container">
        <h1>Article CMS</h1>
        <a href="{{ route('article.create') }}" class="btn btn-primary">Add Article</a>
        <br>
        <br>

        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Slug</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody class="table-body">
               {{-- @foreach($datas as $data)
               <tr>
                   <td>{{ $data->name }}</td>
                   <td>{{ $data->slug }}</td>
                   <td>
                    <div class="btn-group">
                        <a href="#" data-target="#myModal" data-id="{{ $data->id }}" data-toggle="modal" class="btn btn-info btn-detail">Detail</a>
                        <a href="{{ route('article.destroy',$data->id) }}" class="btn btn-danger btn-delete">Delete</a>
                    </div>
                   </td>
               </tr>
               @endforeach  --}}
            </tbody>
        </table>

        <div class="text-center" style="margin-bottom: 50px;">
            <p>Page positon: <span class="page-position">1</span></p>
            <button class="btn btn-primary previous" style="margin-right: 10px;"><</button>
            <button class="btn btn-primary next">></button>
        </div>
    </div>
      
    <div class="modal fade" id="myModal" tab-index="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Article Detail</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
            
                <!-- Modal body -->
                <div class="modal-body">
                    <p>Article Name: <span class="article-name"></span></p>
                    <p>Article Name: <span class="article-slug"></span></p>
                    <p>Creator: <span class="article-creator"></span></p>

                    <a href="" class="article-href" target="_blank">See Page</a>
                </div>
            
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</body>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script>
    let page = 1;
    const number_of_page = {{ $number_of_pages }};
    $(document).ready(function () {
        fetchArticle();
        checkPageButton();
    });
    
    /*
    * For fetching data 
    */
    function fetchArticle() {
        fetch('article/list?skip=' + page)
        .then(res => {
            res.json().then(data => {
                populateTable(data);
            });
        });
    }

    /*
    * For fetching article detail
    */
    function fetchArticleDetail(id) {
        fetch('article/detail?id=' + id,{
        }).then(response=>{
            return response.json()
        }).then(data=>{
            if (data['code'] == 200) {
                console.log(data);
                $(".article-name").text(data['data'][0]['name']);
                $(".article-slug").text(data['data'][0]['slug']);
                $(".article-creator").text(data['data'][0]['creator']['name']);
                $(".article-href").attr('href', '{{ url('article/') }}' + '/' + data['data'][0]['id']);
            }
        });
    }

    /*
    * Populate data on table
    */
    function populateTable(data) {
        const table_data = data.map(data => {
            return '<tr><td>'+ data.name +'</td><td>'+ data.slug +'</td><td><div class="btn btn-group"><button class="btn btn-info btn-detail" data-id="'+ data.id +'">Detail</button><button class="btn btn-danger btn-delete" data-id="'+ data.id +'">Delete</button></div></td></tr>'
        });
        $(".table-body").html(table_data);
    }

    /*
    * Move to the previous Page
    */
    $(".previous").click(function() {
        page--;
        fetchArticle();
        $('.page-position').text(page);
        checkPageButton();
    });

    /*
    * Move to the next Page
    */
    $(".next").click(function() {
        page++;
        fetchArticle();
        $('.page-position').text(page);
        checkPageButton();
    });

    /*
    * Check Page Button
    */
    function checkPageButton() {
        if (page == 1) {
           $('.previous').toggle();
           $('.next').show();
        }else{
            if (page == number_of_page) {
                $('.next').toggle();
            }else{
               $('.next').show();
               $('.previous').show();
           }
        }
        console.log(number_of_page);
        console.log(page);
    }

    /*
    * Click to Show Detail of Article
    */
    $(document).on('click','.btn-detail',function (e) {
       e.stopPropagation();
       e.stopImmediatePropagation();
       var id = $(this).attr("data-id");
       fetchArticleDetail(id);
       $('#myModal').modal('show');
    });

    /*
    * Click to Delete Article
    */
    $(document).on('click','.btn-delete',function (e) {
        e.stopPropagation();
        e.stopImmediatePropagation();
        let href = $(this).attr('data-id');
        if (confirm('Anda yakin ingin menghapus?')) {
            fetch('article/' + href, {
                method: 'DELETE',
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}",
                }
            }).then(response => {
                fetchArticle();
            })
        }
    });

</script>
</html>